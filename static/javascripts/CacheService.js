/**
 * Created by worldwide on 8/16/2015.
 */
(function (){
    'use strict';

    angular.module('editorApp.CacheService')
        .service('CacheService', CacheServiceFun);

    CacheServiceFun.$inject=['$sessionStorage'];

    function CacheServiceFun($sessionStorage){


        this.setCache = function (key, value){
            window.sessionStorage.setItem(key,  JSON.stringify(value));
        }

        this.getCache = function (key){
            return JSON.parse(window.sessionStorage.getItem(key));
        }

        this.isCache = function(key){
            if(window.sessionStorage.getItem(key) != null){
                return true;
            }else{
                return false;
            }
        }

        this.removeCache = function (key){
            window.sessionStorage.removeItem(key);
        }
    }

})();