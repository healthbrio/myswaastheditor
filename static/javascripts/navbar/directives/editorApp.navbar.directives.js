/**
 * Created by worldwide on 8/16/2015.
 */
(function (){
    'use strict';
    angular.module('editorApp.navbar.directives')
.directive('navDirective',navDirectiveFun);
	
	function navDirectiveFun(){
		var directive={};
		directive.restrict = 'EA';
		directive.controller = 'NavbarController';
		directive.controllerAs = 'navbar_vm';
		directive.templateUrl = "static/templates/navbar/navbar.html";
		return directive;
	}
})();