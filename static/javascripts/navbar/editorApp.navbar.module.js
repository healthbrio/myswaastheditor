/**
 * Created by worldwide on 8/16/2015.
 */
(function (){
    'use strict';

    angular.module('editorApp.navbar',[
        'editorApp.navbar.controllers',
        'editorApp.navbar.directives',
        'editorApp.navbar.services'
    ]);

    angular.module('editorApp.navbar.controllers', []);
    angular.module('editorApp.navbar.directives', []);
    angular.module('editorApp.navbar.services', []);

})();