/**
 * Created by worldwide on 8/16/2015.
 */
(function (){
    'use strict';

    angular.module('editorApp.navbar.controllers')
        .controller('NavbarController', navbarControllerFun);

    navbarControllerFun.$inject = ['$scope', '$http', '$state', 'editorApp.config', 'CacheService'];

    function navbarControllerFun($scope, $http, $state, config, CacheService){
        var vm = this;
        /*Logging out form the editor dashboard*/
        vm.logout =function(){
            //alert("logout calls");
            if(CacheService.isCache("client_secret")){
                CacheService.removeCache("client_secret");
            }
            $state.go("home");
        }   
        
    }
})();