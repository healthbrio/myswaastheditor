(function (){
    'use strict';

    angular.module('editorApp.HttpService')
        .service('HttpService', HttpServiceFun);
        
       HttpServiceFun.$inject=['$http','CacheService'];
       
        function HttpServiceFun($http,CacheService){
            
            var authorize=JSON.parse(CacheService.getCache("client_secret"));
            var token =authorize[0].token.access_token;
            var token_type=authorize[0].token.token_type;
          
            
            
            return{   
                        
                  PrivateServiceGet: PrivateServiceGetFun,
                  PublicServiceGet: PublicServiceGetFun,
                  PrivateServicePost:PrivateServicePostFun,
                  PublicServicePost:PublicServicePostFun          
            }
            
            function PrivateServiceGetFun(url_){
                
                 return $http({
                    url :url_,
                    method: 'GET',
                    headers : {          
                         'Content-Type' : 'application/json', 
                         'Authorization' : token_type+" "+token  
                    }                                       
                });
              
            }
            function PublicServiceGetFun(url_){
                
                return $http({
                    url :url_,
                    method: 'GET',
                    headers : {          
                         'Content-Type' : 'application/json', 
                         'Authorization' : token_type+" "+token  
                    }      
                                                    
                });
            }
            function PrivateServicePostFun(url_, data_){
                
                var authorize=JSON.parse(CacheService.getCache('client_secret'));
                var token =authorize[0].token.access_token;
                //alert(JSON.stringify(data_));
                                
                 return $http({
                    url :url_,
                    method: 'POST',
                    data: data_,
                    headers : {        
                         'Content-Type' : 'application/json',
                         'Authorization' : token_type+" "+token
                    }                                       
                });
              
            }
            function PublicServicePostFun(url_, data_){
                
                  return $http({
                    url :url_,
                    method: 'POST',
                    data: data_,
                    headers : {        
                         'Content-Type' : 'application/json',
                         'Authorization' : token_type+" "+token
                    }                                       
                });
            }
          
        }


})();















