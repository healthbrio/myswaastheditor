/**
 * Created by IDCS12 on 3/18/2015.
 */
(function () {
    angular.module('editorApp.home.controller')
        .controller('HomeController', homeControllerFun);


    homeControllerFun.$inject = ['$scope', '$http', '$state', 'editorApp.config', '$interval', 'HomeService', '$location', '$window', 'AuthenticationService', '$base64'];
            
    function homeControllerFun($scope, $http, $state, config, $interval, $location, $window, HomeService, AuthenticationService, $base64) {
        var vm = this;
        //alert("primary index calls");

        vm.login = function (username, password) {
            //alert("login called" + username + password);
            // alert(config.basePath);

           
            if (username !== undefined && password !== undefined) {
                var result = $base64.encode(username + ':' + password);

                $http({
                    url: 'http://52.74.163.60/client/login/',
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Basic ' + result

                    }
                }).success(function (response) {
                    // alert("successiful authentication");
                    password = "";
                    AuthenticationService.sendEditorToken(response);
                    AuthenticationService.getEditorEmail(username);
                    
                    //alert("has_edit: "+response[0].info.has_edit);
                    //alert("is_rules: "+response[0].info.is_rules);

                    if (response[0].info.is_rules && response[0].info.has_edit == true) {
                        $state.go('editor');
                    } else if (response[0].info.is_rules == true) {
                        $state.go('details');
                    } else {
                        $state.go('policy');
                    }

                }).error(function (error, status) {
                    password = "";
                    if (status == 401) {
                        alert("You Are UNAUTHORIZED Person. PLEASE TRY AGAIN");
                    } else {
                        alert('Oh snap! Connection Fail. TRY AGAIN');
                    }
                })

            } else if (username !== undefined) {
                alert("please enter password");
                password = "";
            } else if (password !== undefined) {
                alert("please enter username");
                username = "";
            }
            else {
                alert("please enter username and password");
                password = "";
                username = "";
            }
        }
    }

})();

