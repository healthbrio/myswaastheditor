/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';
    angular.module('editorApp.home.services')
        .service('HomeService', homeServiceFun);

    angular.module('editorApp.home.services')
        .service('AuthenticationService', AuthenticationServiceFun );


    AuthenticationServiceFun.$inject = ['CacheService',];


    function homeServiceFun()
    {
    	
    }

    function AuthenticationServiceFun(CacheService){
        this.sendEditorToken = function(token){
            //alert(JSON.stringify(token));
            CacheService.setCache("client_secret", JSON.stringify(token));

        }
         this.getEditorEmail = function(email){
              CacheService.setCache("editor_email", JSON.stringify(email));
         }
    }


    
})();