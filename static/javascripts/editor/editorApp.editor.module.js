/**
 * Created by worldwide on 8/16/2015.
 */
(function (){
    'use strict';

    angular.module('editorApp.editor',[
        'editorApp.editor.controllers',
        'editorApp.editor.directives',
        'editorApp.editor.services'
    ]);

    angular.module('editorApp.editor.controllers', []);
    angular.module('editorApp.editor.directives', []);
    angular.module('editorApp.editor.services', []);

})();