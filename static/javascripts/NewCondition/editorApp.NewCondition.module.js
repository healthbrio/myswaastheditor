(function (){
    'use strict';

    angular.module('editorApp.NewCondition',[
        'editorApp.NewCondition.controllers',
        'editorApp.NewCondition.directives',
        'editorApp.NewCondition.services'
    ]);

    angular.module('editorApp.NewCondition.controllers', []);
    angular.module('editorApp.NewCondition.directives', []);
    angular.module('editorApp.NewCondition.services', []);

})();