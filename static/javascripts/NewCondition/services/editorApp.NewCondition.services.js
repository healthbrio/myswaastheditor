/**
 * Created by worldwide on 8/16/2015.
 */
(function (){
    'use strict';
    angular.module('editorApp.NewCondition.services')
        .service("NewConditionService",newconditionserviceFun);
        
        newconditionserviceFun.$inject = ['HttpService', 'editorApp.config'];
        
        function newconditionserviceFun(HttpService, config){ 
           
            this.getNewConditionsData = function(params)
	           {
		          return HttpService.PublicServiceGet(config.basePath + 'api/conditions/'+params);
	           }
              this.postEditConditionsData = function(dataJson)
	           { 
		          return HttpService.PublicServicePost(config.basePath + 'api/conditions/edit_condition_save/',dataJson);
	           }   
            
        }
})();   