(function (){
    angular.module('editorApp.NewCondition.controllers')
        .controller('NewConditionController', newconditionControllerFun);
        
        
        newconditionControllerFun.$inject = ['$http', '$scope', '$state', '$stateParams', 'fileReader', 'NewConditionService', 'CacheService'];
        
        
        function newconditionControllerFun($http, $scope, $state, $stateParams, fileReader, NewConditionService, CacheService)
        {
            $scope.ipAddress={};
            $scope.condition = [];
            $scope.NewconditionData=[];
            $scope.editoremail="";
            
            $http.jsonp('http://ipinfo.io/?callback=JSON_CALLBACK')
                .success(function(param){
                    $scope.ipAddress=param.ip;
                    }); 

            if(CacheService.isCache("editor_email")){
                $scope.editoremail = CacheService.getCache("editor_email");
            }else{
                alert("sessions storage deeleted");
            }
            $scope.dataJson=
            {
            "name": "",
            "main_condition": {
            "slug": $stateParams.slug,
            "name": ""
             },
            "common_name": "",
            "description": "",
            "treatment": "",
            "symptoms": "",
            "workup": "",
            "images":[""],
            "videos":[""],
            "created_by" : $scope.editoremail,
            "ip_address" : $scope.ipAddress,
            "new" : false
            };  
      
            $scope.getFile = function()
            {
                $scope.imageSrc=[];
                for(var i=0;i<$scope.file.length;i++)
                    {
                        fileReader.readAsDataUrl($scope.file[i], $scope)
                            .then(function(result){
                                $scope.imageSrc.push(result);
                                });
                     }
            };
        
        
          $scope.download=function() 
          {
            var url=document.getElementById('img').value;    
            $http.get(url, {responseType: "arraybuffer"})
                .success(function(data)
                    {
                          var arrayBufferView = new Uint8Array( data );
                          var blob = new Blob( [ arrayBufferView ], { type: "image/png" } );
                          var urlCreator = window.URL || window.webkitURL;
                          var imageUrl = urlCreator.createObjectURL( blob );
                          var img = document.querySelector( "#photo" );
                          img.src = imageUrl;
                    })
                .error(function(err, status){})
          }
    
    
        NewConditionService.getNewConditionsData($stateParams.slug)
            .success(function(response)
                {
                    $scope.Newcondition = response;  
                    for(var i=0; i<$scope.Newcondition.length; i++)
                        {
                            if($stateParams.slug == $scope.Newcondition[i].slug)
                            {
                               $scope.NewconditionData=$scope.Newcondition[i];
                               break;
                             }
                         }          
                        
                 }) 
           .error(function(error)
                {
                    toastr.warning(error);
                });
                    
                    
        $scope.save = function()
        {
            NewConditionService.postEditConditionsData($scope.dataJson)
                .success(function(response)
                    {
                        toastr.success(response);
                        var condition_list = CacheService.getCache("doctor_condition_list");
                        angular.forEach(condition_list, function(key, value){
                            if(key.main_condition.slug==$stateParams.slug)
                            {
                                condition_list[value].new=false;
                                CacheService.setCache("doctor_condition_list", condition_list);
                             }
                        });
                            
                     })
                .error(function(error,status)
                    {
                        toastr.warning(status + error);
                    });
                        
        }

 
        $scope.next = function()
        {
            var temp_index = 0;
            var condition_list = CacheService.getCache("doctor_condition_list");
            angular.forEach(condition_list, function(key, value){
                if(key.main_condition.slug==$stateParams.slug)
                {
                    var next_condition = condition_list[value+1];
                    if(next_condition.new==true)
                    {
                        $state.go("NewCondition", {slug : next_condition.main_condition.slug});
                    }
                    else{
                        $state.go("EditCondition", {slug : next_condition.main_condition.slug});
                    }
                }
            });
            
        }
        
        
        $scope.previous = function()
        {
            var temp_index = 0;
            var condition_list = CacheService.getCache("doctor_condition_list");
            angular.forEach(condition_list, function(key, value){
                if(key.main_condition.slug==$stateParams.slug)
                {
                    var next_condition = condition_list[value-1];
                    if(next_condition.new==true)
                    {
                        $state.go("NewCondition", {slug : next_condition.main_condition.slug});
                    }
                    else{
                        $state.go("EditCondition", {slug : next_condition.main_condition.slug});
                    }
                }
            });
        }
    
       
    }

     
})();