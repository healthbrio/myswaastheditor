(function(){
    'use strict';
    angular.module('editorApp.policy.services')
        .service('PolicyService', policyServiceFun);
        
        policyServiceFun.$inject = ['HttpService', 'editorApp.config'];
        
         function policyServiceFun(HttpService, config){
            
            this.getPolicy = function(){ 
                
                return HttpService.PrivateServiceGet(config.basePath + "api/doctors/confirm_rules/");
            }
         }
               
})();