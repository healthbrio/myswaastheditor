(function(){
    angular.module('editorApp.policy.controllers')
	.controller('PolicyController',PolicyControllerFun);
	
	PolicyControllerFun.$inject = ['$scope', '$http', '$state', 'PolicyService'];
	
	function  PolicyControllerFun($scope, $http, $state, PolicyService){
			
		$scope.next = function(){
			
			PolicyService.getPolicy().success(function(response){
				 alert(response);
				 $state.go('details');
			 }).error(function(response){
				 alert("error");
			 })
			
		}
	}
	
})();