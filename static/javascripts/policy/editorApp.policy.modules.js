(function(){
    'use strict';

    angular.module('editorApp.policy',[
        'ui.bootstrap',
        'ngAnimate',
        'editorApp.policy.controllers',
        'editorApp.policy.directives',
        'editorApp.policy.services'
    ]);
    
    
    angular.module('editorApp.policy.controllers',['ui.bootstrap','ngAnimate']);
    angular.module('editorApp.policy.directives',[]);
    angular.module('editorApp.policy.services',[]);
})();