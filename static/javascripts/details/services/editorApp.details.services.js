(function(){
    'use strict';
    angular.module('editorApp.details.services')
        .service('DetailsService', detailsServiceFun);
        
        detailsServiceFun.$inject = ['HttpService', 'editorApp.config'];
        
         function detailsServiceFun(HttpService, config){
             
             this.getDetails = function(data1){
                 
                 return HttpService.PrivateServicePost(config.basePath + "api/doctors/edit_profile/",data1);
                 
             }
         }
               
})();