(function(){
    'use strict';

    angular.module('editorApp.details',[
        'ui.bootstrap',
        'ngAnimate',
        'editorApp.details.controllers',
        'editorApp.details.directives',
        'editorApp.details.services'
    ]);
    
    
    angular.module('editorApp.details.controllers',['ui.bootstrap','ngAnimate']);
    angular.module('editorApp.details.directives',[]);
    angular.module('editorApp.details.services',[]);

})();