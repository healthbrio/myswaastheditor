(function () {
    'use strict';

    angular.module('editorApp.EditCondition.controllers')
        .controller('EditConditionController', editconditionControllerFun);


    editconditionControllerFun.$inject = ['$http', '$scope', '$state', '$stateParams', 'fileReader', 'EditConditionService', 'CacheService'];

    function editconditionControllerFun($http, $scope, $state, $stateParams, fileReader, EditConditionService, CacheService) {

        $scope.ipAddress = {};
        $scope.Description_data=[];
        $scope.editoremail=""
        $http.jsonp('http://ipinfo.io/?callback=JSON_CALLBACK').success(function (param) {
            $scope.ipAddress = param.ip;
        });


        if(CacheService.isCache("editor_email")){
            $scope.editoremail = CacheService.getCache("editor_email");
            alert($scope.editoremail);
        }else{
            alert("sessions storage deeleted");
        }

        $scope.dataJson =
        {
            "name": "",
            "main_condition": {
                "slug": $stateParams.slug,
                "name": ""
            },
            "common_name": "",
            "description": "",
            "treatment": "",
            "symptoms": "",
            "workup": "",
            "images": [""],
            "videos": [""],
            "created_by": $scope.editoremail,
            "ip_address": $scope.ipAddress,
            "new": false
        };  
                   
           
        $scope.getFile = function () {
            $scope.imageSrc = [];
            for (var i = 0; i < $scope.file.length; i++) {
                fileReader.readAsDataUrl($scope.file[i], $scope)
                    .then(function (result) {
                        $scope.imageSrc.push(result);
                    });
            }
        };

        $scope.download = function () {
            var url = document.getElementById('img').value;
            $http.get(url, { responseType: "arraybuffer" })
                .success(function (data) {
                    var arrayBufferView = new Uint8Array(data);
                    var blob = new Blob([arrayBufferView], { type: "image/png" });
                    var urlCreator = window.URL || window.webkitURL;
                    var imageUrl = urlCreator.createObjectURL(blob);
                    var img = document.querySelector("#photo");
                    img.src = imageUrl;
                })
                .error(function (err, status) { })
        };


        EditConditionService.getEditConditionsData($stateParams.slug)
            .success(function (response) {
                $scope.dataJson = response;
            })
            .error(function (error) {
                toastr.warning("error " + error);
            });
            
       EditConditionService.getDescriptionData($stateParams.slug)
            .success(function(response){
                $scope.Description_data = response;
            })
            .error(function(error){
                toastr.warning("error " + error);
            });
            


        $scope.save = function () {
            $scope.dataJson.created_by = "editor@gmail.com";
            $scope.dataJson.ip_address = $scope.ipAddress;
            $scope.dataJson.new = false;
            EditConditionService.postEditConditionsData($scope.dataJson)
                .success(function (response) {
                      toastr.success(response);
                })
                .error(function (error, status) {
                    toastr.warning(status + error);
                });

        }


        $scope.next = function () {
            var temp_index = 0;
            var condition_list = CacheService.getCache("doctor_condition_list");
            angular.forEach(condition_list, function (key, value) {
                if (key.main_condition.slug == $stateParams.slug) {
                    var next_condition = condition_list[value + 1];
                    if (next_condition.new == true) {
                        $state.go("NewCondition", { slug: next_condition.main_condition.slug });
                    }
                    else {
                        $state.go("EditCondition", { slug: next_condition.main_condition.slug });
                    }
                }
            });

        }
        $scope.previous = function () {
            var temp_index = 0;
            var condition_list = CacheService.getCache("doctor_condition_list");
            angular.forEach(condition_list, function (key, value) {
                if (key.main_condition.slug == $stateParams.slug) {
                    var next_condition = condition_list[value - 1];
                    if (next_condition.new == true) {
                        $state.go("NewCondition", { slug: next_condition.main_condition.slug });
                    }
                    else {
                        $state.go("EditCondition", { slug: next_condition.main_condition.slug });
                    }
                }
            });
        }

    }


})();