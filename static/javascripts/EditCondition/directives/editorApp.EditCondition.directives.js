/**
 * Created by worldwide on 8/16/2015.
 */
(function (){
    'use strict';
    angular.module('editorApp.EditCondition.directives')
    
           .directive('ngFileSelect',ngFileSelectFun);
             function ngFileSelectFun(){
    
        return {
            link: function($scope,el){
            $scope.file=[];
            el.bind("change", function(e){
            for(var i=0;i<(e.srcElement || e.target).files.length;i++)
            {
                //alert((e.srcElement || e.target).files[i]);
                $scope.file.push((e.srcElement || e.target).files[i]);
                
            }
                $scope.getFile();
            })
            
            }
            
        }
      
      
    }
})();