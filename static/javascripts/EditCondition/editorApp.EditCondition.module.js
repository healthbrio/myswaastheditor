(function (){
    'use strict';

    angular.module('editorApp.EditCondition',[
        'editorApp.EditCondition.controllers',
        'editorApp.EditCondition.directives',
        'editorApp.EditCondition.services'
    ]);

    angular.module('editorApp.EditCondition.controllers', []);
    angular.module('editorApp.EditCondition.directives', []);
    angular.module('editorApp.EditCondition.services', []);

})();