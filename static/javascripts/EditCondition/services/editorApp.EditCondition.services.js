/**
 * Created by worldwide on 8/16/2015.
 */
(function (){
    'use strict';
    angular.module('editorApp.EditCondition.services')
        .service("EditConditionService",editconditionserviceFun);
        
        editconditionserviceFun.$inject = ['HttpService', 'editorApp.config'];
        
        function editconditionserviceFun(HttpService, config){ 
          
             this.getEditConditionsData = function(params)
	           {
                   
		          return HttpService.PublicServiceGet(config.basePath + 'api/conditions/edit_condition/'+params);
	           }
               this.postEditConditionsData = function(dataJson)
	           {
                   
		          return HttpService.PublicServicePost(config.basePath + 'api/conditions/edit_condition_save/',dataJson);
	           }
               this.getDescriptionData = function(params)
	           {
		          return HttpService.PublicServiceGet(config.basePath + 'api/conditions/'+params);
	           }
               
        }
})();