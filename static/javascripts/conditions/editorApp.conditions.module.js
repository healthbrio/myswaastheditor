
(function (){
    'use strict';

    angular.module('editorApp.conditions',[
        'ui.bootstrap',
        'ngAnimate',
        'editorApp.conditions.controllers',
        'editorApp.conditions.directives',
        'editorApp.conditions.services',
    ]);

    angular.module('editorApp.conditions.controllers', ['ui.bootstrap','ngAnimate']);
    angular.module('editorApp.conditions.directives', []);
    angular.module('editorApp.conditions.services', []);

})();