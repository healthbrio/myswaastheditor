(function () {
    angular.module('editorApp.conditions.controllers')
        .controller('ConditionsController', conditionsControllerFun);

    conditionsControllerFun.$inject = ['$http', '$scope', '$log', 'ConditionsService', 'CacheService'];

    function conditionsControllerFun($http, $scope, $log, ConditionsService, CacheService) {
        $scope.itemsPerPage = 25;
        $scope.currentPage = 1;
        $scope.data = [];

        ConditionsService.getMyConditionsList()
            .success(function (response) {
              
                $scope.data = response;
                CacheService.setCache("doctor_condition_list", response);
                $scope.totalItems = $scope.data.length;
            })
            .error(function (error) {
                  toastr.warning(error);
            });
    }

})();