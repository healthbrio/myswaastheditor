/**
 * Created by worldwide on 8/16/2015.
 */
(function (){
    'use strict';
    angular.module('editorApp.conditions.services')
        .service("ConditionsService",conditionsserviceFun);
        
        conditionsserviceFun.$inject = ['HttpService', 'editorApp.config'];
        
        function conditionsserviceFun(HttpService, config){
            this.getMyConditionsList = function()
	           {
		          return HttpService.PublicServiceGet(config.basePath + 'api/conditions/editor_condition/');
	           }
            
        }
})();