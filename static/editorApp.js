

/*angular.module('myApp', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider){
        $routeProvider.when('/symptoms', {templateUrl: 'templates/symptoms/symptoms.html'})
        .when('/conditions', {templateUrl: 'templates/conditions/conditions.html'})

    }]);*/



(function () {
    'use strict';

    var app=angular
    .module('editorApp', [
      'editorApp.config',
      //'ngAnimate',
      'editorApp.routes',
      'editorApp.home',
      'editorApp.editor',
      'editorApp.policy',
      'editorApp.details',
      'editorApp.conditions',
      'editorApp.NewCondition',
      'editorApp.EditCondition',
      'editorApp.navbar',
      'editorApp.HttpService',
      'editorApp.CacheService'
    ]);

    angular.module('editorApp.config', []);
    angular.module('editorApp.CacheService', []);
    angular.module('editorApp.HttpService', []);

    // Module specific configuration
    angular.module('editorApp.config')
        .value('editorApp.config', {
            basePath: 'http://52.74.163.60/' // Set your base path here
        });


    angular.module('editorApp.routes', ['ngRoute', 'ui.router', 'base64', 'ngStorage']).run(function($rootScope) {
    /*$rootScope.static_path = 'https://healthbrio.s3.amazonaws.com/';*/
    $rootScope.static_path = 'http://127.0.0.1:8000/static/';
    });

})();






