(function () {
  'use strict';

  angular
    .module('editorApp.routes')
    .config(indexConfig);
   
    
   

  indexConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
 
  
/**
   * @name config
   * @desc Define valid application routes*/

   //***  stateProvider for Symptoms*****
  window.static_path = 'http://127.0.0.1:8000/static/';
  /*window.static_path = 'https://healthbrio.s3.amazonaws.com/';*/

  function indexConfig($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            controller:'HomeController',
            controllerAs:'home_vm',
            templateUrl: static_path+'templates/home/home.html'
        })
        .state('policy', {
            url: '/policy',
            controller:'PolicyController',
            controllerAs:'policy_vm',
            templateUrl: static_path+'templates/policy/policy.html'
        })
        .state('editor',{
            url: '/editor',
            controller :'EditorController',
            controllerAs:'editor_vm',
            templateUrl: static_path+'templates/editor/editor.html'
        })
          .state('details',{
            url: '/details',
            controller:'DetailsController',
            controllerAs:'detaisCtrl_vm',
            templateUrl: static_path+'templates/details/details.html'
          })
        .state('conditions',{
          url: '/conditions', 
          controller:'ConditionsController',       
          controllerAs:'conditions_vm',    
          templateUrl: static_path + 'templates/conditions/conditions.html'
        })
        .state('NewCondition',{
          url: '/condition/new/:slug',
          params : { index_of_condition :  null}, 
          controller : 'NewConditionController', 
          controllerAs:'newcondition_vm',          
          templateUrl: static_path + 'templates/condition/NewCondition.html'
        })
          .state('EditCondition',{
          url: '/condition/edit/:slug',
          params : { index_of_condition :  null}, 
          controller : 'EditConditionController', 
          controllerAs:'editcondition_vm',          
          templateUrl: static_path + 'templates/condition/EditCondition.html'
        });
        
  }


 
})();

